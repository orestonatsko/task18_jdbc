 create datababase if not  exists student_db;
 use student_db;
CREATE TABLE  `address` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `city` VARCHAR(45) NOT NULL,
  `street` VARCHAR(45) NULL,
  `numb_building` INT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

CREATE TABLE  `st_group` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

CREATE TABLE  `student` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(45)NOT NULL,
  `second_name` VARCHAR(45) NOT NULL,
  `address` INT NULL,
  `group_id` INT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT FOREIGN KEY (`address`)
    REFERENCES `address` (`id`),
  CONSTRAINT FOREIGN KEY (`group_id`)
    REFERENCES `st_group` (`id`))
ENGINE = InnoDB;

CREATE TABLE `discipline` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `discipline` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

CREATE TABLE  `student_db`.`student_discipline` (
  `student_id` INT NOT NULL,
  `discipline_id` INT NOT NULL,
  PRIMARY KEY (`student_id`, `discipline_id`),
  CONSTRAINT FOREIGN KEY (`student_id`)
    REFERENCES `student` (`id`),
  CONSTRAINT FOREIGN KEY (`discipline_id`)
    REFERENCES `discipline` (`id`))
ENGINE = InnoDB;

#---------------------------------------------------------+
#------------------------ DATA ---------------------------|
#---------------------------------------------------------+

-- data for address table
insert into address(city, street, numb_building)
	values('Lviv', 'Horodotska', 45),
		  ('Lviv', 'Levandivska', 145),
		  ('Ternopil','Kotsyubynskoho', 36),
		  ('Rivne', 'Popovycha', 32),
      ('Ternopil','Stepana Bandery', 44),
      ('Ternopil','Stepana Bandery', 14),
      ('Rivne', 'Soborna', 61),
      ('Rivne', 'Soborna', 98);

-- data for group table
insert into st_group(name)
	values('КТ-1'),
		    ('КТ-2'),
        ('ІБ-1');
          
-- data for discipline table
insert into discipline(discipline)
		values('Алгоритми і методи обчислень'),
			    ('Комп’ютерна електроніка'),
          ('Програмування'),
          ('Системне програмування');
              
-- data for student table
insert into student(first_name, second_name, address, group_id)
			values('Andrii', 'Vestrich', 1, 1),
				  ('Igor', 'Mernik', 2, 1),
          ('Oleh', 'Drezda', 3, 1),
          ('Ira', 'Kolich', 4, 2),
          ('Maksim', 'Melnik', 5, 3),
          ('Olga', 'Fran', 6, 2),
          ('Oleh', 'Dred', 7, 2),
          ('Vitaliy', 'Brezlid', 8, 3);