package com.epamcourses.orestonatsko.DAO;

import com.epamcourses.orestonatsko.model.DisciplineEntity;

public interface DisciplineDAO extends GeneralDAO<DisciplineEntity, Integer> {
}
