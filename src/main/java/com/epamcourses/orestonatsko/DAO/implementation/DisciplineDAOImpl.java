package com.epamcourses.orestonatsko.DAO.implementation;

import com.epamcourses.orestonatsko.DAO.DisciplineDAO;
import com.epamcourses.orestonatsko.connection.ConnectionManager;
import com.epamcourses.orestonatsko.handler.Transformer;
import com.epamcourses.orestonatsko.model.DisciplineEntity;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DisciplineDAOImpl implements DisciplineDAO {
    private static final String GET_ALL = "SELECT * FROM discipline";
    private static final String GET_BY_ID = "SELECT * FROM discipline WHERE id=?";
    private static final String INSERT_INTO = "INSERT INTO discipline(discipline) VALUES (?)";
    private static final String DELETE = "DELETE FROM discipline WHERE id=?";
    private static final String UPDATE = "UPDATE discipline SET discipline=? WHERE id=?";

    @Override
    public List<DisciplineEntity> getAll() throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        List<DisciplineEntity> disciplines = new ArrayList<>();
        try (Statement st = connection.createStatement()) {
            try (ResultSet rs = st.executeQuery(GET_ALL)) {
                while (rs.next()) {
                    disciplines.add((DisciplineEntity) new Transformer(DisciplineEntity.class).transformToEntity(rs));
                }
            }
        }
        return disciplines;
    }

    @Override
    public DisciplineEntity getById(Integer id) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        DisciplineEntity discipline = null;
        try (PreparedStatement st = connection.prepareStatement(GET_BY_ID)) {
            st.setInt(1, id);
            try (ResultSet rs = st.executeQuery()) {
                while (rs.next()) {
                    discipline = (DisciplineEntity) new Transformer(DisciplineEntity.class).transformToEntity(rs);
                }
            }
        }
        return discipline;
    }

    @Override
    public int create(DisciplineEntity entity) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement st = connection.prepareStatement(INSERT_INTO)) {
            st.setString(1, entity.getDiscipline());
            return st.executeUpdate();
        }
    }

    @Override
    public int delete(Integer id) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement st = connection.prepareStatement(DELETE)) {
            st.setInt(1, id);
            return st.executeUpdate();
        }
    }

    @Override
    public int update(DisciplineEntity entity) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement st = connection.prepareStatement(UPDATE)) {
            st.setString(1, entity.getDiscipline());
            st.setInt(2, entity.getId());
            return st.executeUpdate();
        }
    }
}
