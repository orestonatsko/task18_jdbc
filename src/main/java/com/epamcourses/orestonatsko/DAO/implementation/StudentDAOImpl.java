package com.epamcourses.orestonatsko.DAO.implementation;

import com.epamcourses.orestonatsko.DAO.StudentDAO;
import com.epamcourses.orestonatsko.connection.ConnectionManager;
import com.epamcourses.orestonatsko.handler.Transformer;
import com.epamcourses.orestonatsko.model.StudentEntity;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class StudentDAOImpl implements StudentDAO {
    private static final String GET_ALL = "SELECT * FROM student";
    private static final String GET_BY_ID = "SELECT * FROM student WHERE id=?";
    private static final String GET_BY_NAME = "SELECT * FROM student WHERE first_name=?";
    private static final String INSERT_INTO = "INSERT INTO  student(first_name, second_name) VALUES(?, ?)";
    private static final String UPDATE = "UPDATE student SET first_name=?, second_name=? WHERE id=?";
    private static final String DELETE = "DELETE FROM student WHERE id=?";

    @Override
    public StudentEntity getByName(String name) throws SQLException {
        StudentEntity student = null;
        Connection connection = ConnectionManager.getConnection();
        try(PreparedStatement st = connection.prepareStatement(GET_BY_NAME)){
            st.setString(1, name);
            try(ResultSet rs = st.executeQuery()) {
                while (rs.next()) {
                    student = (StudentEntity) new Transformer(StudentEntity.class).transformToEntity(rs);
                }
            }
        }
        return student;
    }

    @Override
    public List<StudentEntity> getAll() throws SQLException {
        List<StudentEntity> students = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(GET_ALL)) {
                while (resultSet.next()) {
                    students.add((StudentEntity) new Transformer(StudentEntity.class).transformToEntity(resultSet));
                }
            }
        }
        return students;
    }

    @Override
    public StudentEntity getById(Integer id) throws SQLException {
        StudentEntity student = null;
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement st = connection.prepareStatement(GET_BY_ID)) {
            st.setInt(1, id);
            try (ResultSet rs = st.executeQuery()) {
                while (rs.next()) {
                    student = (StudentEntity) new Transformer(StudentEntity.class).transformToEntity(rs);
                }
            }
        }
        return student;
    }

    @Override
    public int create(StudentEntity entity) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT_INTO)) {
            preparedStatement.setString(1, entity.getFirstName());
            preparedStatement.setString(2, entity.getSecondName());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int delete(Integer id) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement st = connection.prepareStatement(DELETE)) {
            st.setInt(1, id);
            return st.executeUpdate();
        }
    }

    @Override
    public int update(StudentEntity entity) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement st = connection.prepareStatement(UPDATE)) {
            st.setString(1, entity.getFirstName());
            st.setString(2, entity.getSecondName());
            st.setInt(3, entity.getId());

            return st.executeUpdate();
        }
    }
}
