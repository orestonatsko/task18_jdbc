package com.epamcourses.orestonatsko.DAO.implementation;

import com.epamcourses.orestonatsko.DAO.AddressDAO;
import com.epamcourses.orestonatsko.connection.ConnectionManager;
import com.epamcourses.orestonatsko.handler.Transformer;
import com.epamcourses.orestonatsko.model.AddressEntity;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class AddressDAOImpl implements AddressDAO {
    private static final String GET_ALL = "SELECT * FROM address";
    private static final String GET_BY_ID = "SELECT * FROM address WHERE id=?";
    private static final String INSERT_INTO = "INSERT INTO address(city, street, numb_building) VALUES(?,?,?)";
    private static final String DELETE = "DELETE FROM address WHERE id=?";
    private static final String UPDATE = "UPDATE address SET city=?, street=?, numb_building=? WHERE id=?";


    @Override
    public List<AddressEntity> getAll() throws SQLException {
        List<AddressEntity> addresses = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            try (ResultSet rs = statement.executeQuery(GET_ALL)) {
                while (rs.next()) {
                    addresses.add((AddressEntity) new Transformer(AddressEntity.class).transformToEntity(rs));
                }
            }
        }
        return addresses;
    }

    @Override
    public AddressEntity getById(Integer id) throws SQLException {
        AddressEntity address = null;
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement st = connection.prepareStatement(GET_BY_ID)) {
            st.setInt(1, id);
            try (ResultSet rs = st.executeQuery()) {
                while (rs.next()) {
                    address = (AddressEntity) new Transformer(AddressEntity.class).transformToEntity(rs);
                }
            }
        }
        return address;
    }

    @Override
    public int create(AddressEntity entity) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement st = connection.prepareStatement(INSERT_INTO)) {
            st.setString(1, entity.getCity());
            st.setString(2, entity.getStreet());
            st.setInt(3, entity.getNumbBuilding());
            return st.executeUpdate();
        }
    }

    @Override
    public int delete(Integer id) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement st = connection.prepareStatement(DELETE)) {
            st.setInt(1, id);
            return st.executeUpdate();
        }
    }

    @Override
    public int update(AddressEntity entity) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement st = connection.prepareStatement(UPDATE)) {
            st.setString(1, entity.getCity());
            st.setString(2, entity.getStreet());
            st.setInt(3, entity.getNumbBuilding());
            st.setInt(4, entity.getId());
            return st.executeUpdate();
        }
    }
}
