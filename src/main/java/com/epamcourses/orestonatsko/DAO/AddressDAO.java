package com.epamcourses.orestonatsko.DAO;

import com.epamcourses.orestonatsko.model.AddressEntity;

public interface AddressDAO extends GeneralDAO<AddressEntity, Integer> {
}
