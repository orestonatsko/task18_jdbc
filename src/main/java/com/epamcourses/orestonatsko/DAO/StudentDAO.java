package com.epamcourses.orestonatsko.DAO;

import com.epamcourses.orestonatsko.model.StudentEntity;

import java.sql.SQLException;

public interface StudentDAO extends GeneralDAO<StudentEntity, Integer> {
    StudentEntity getByName(String name) throws SQLException;
}
