package com.epamcourses.orestonatsko.model;

import com.epamcourses.orestonatsko.model.annotations.Column;
import com.epamcourses.orestonatsko.model.annotations.PrimaryKey;
import com.epamcourses.orestonatsko.model.annotations.Table;

@Table(name = "student")
public class StudentEntity {
    @PrimaryKey
    @Column(name = "id")
    private Integer id;

    @Column(name = "first_name", length = 45)
    private String firstName;

    @Column(name = "second_name", length = 45)
    private String secondName;

    @Column(name = "address")
    private Integer address;

    @Column(name = "group_id")
    private Integer group;

    public StudentEntity(){}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public Integer getAddress() {
        return address;
    }

    public void setAddress(Integer address) {
        this.address = address;
    }

    public Integer getGroup() {
        return group;
    }

    public void setGroup(Integer group) {
        this.group = group;
    }

    @Override
    public String toString() {
        return String.format("id: %-2d %-8s %s", id, firstName, secondName);
    }
}
