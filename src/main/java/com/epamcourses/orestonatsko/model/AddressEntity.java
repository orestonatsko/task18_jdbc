package com.epamcourses.orestonatsko.model;

import com.epamcourses.orestonatsko.model.annotations.Column;
import com.epamcourses.orestonatsko.model.annotations.PrimaryKey;
import com.epamcourses.orestonatsko.model.annotations.Table;

@Table(name = "Address")
public class AddressEntity {
    @PrimaryKey
    @Column(name = "id")
    private Integer id;
    @Column(name = "city")
    private String city;
    @Column(name = "street")
    private String street;
    @Column(name = "numb_building")
    private Integer numbBuilding;

    public AddressEntity() {
    }

    public AddressEntity(String city, String street, Integer numbBuilding) {
        this.city = city;
        this.street = street;
        this.numbBuilding = numbBuilding;
    }

    public AddressEntity(Integer id, String city, String street, Integer numbBuilding) {
        this.id = id;
        this.city = city;
        this.street = street;
        this.numbBuilding = numbBuilding;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public Integer getNumbBuilding() {
        return numbBuilding;
    }

    public void setNumbBuilding(Integer numbBuilding) {
        this.numbBuilding = numbBuilding;
    }

    @Override
    public String toString() {
        return String.format("id: %-5d %-10s %-18s %s", id, city, street, numbBuilding);
    }
}
