package com.epamcourses.orestonatsko.model.metadata;

import java.util.ArrayList;
import java.util.List;

public class TableMetaData {
    private String DBName;
    private String tableName;
    private List<ColumnMetaData> columnMetaData = new ArrayList<>();

    public String getDBName() {
        return DBName;
    }

    public void setDBName(String DBName) {
        this.DBName = DBName;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public List<ColumnMetaData> getColumnMetaData() {
        return columnMetaData;
    }

    public void setColumnMetaData(List<ColumnMetaData> columnMetaData) {
        this.columnMetaData = columnMetaData;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("TABLE: " + tableName + "\n");
        for (ColumnMetaData column : columnMetaData) {
            stringBuilder.append(column + "\n");
        }

        return stringBuilder.toString();
    }
}
