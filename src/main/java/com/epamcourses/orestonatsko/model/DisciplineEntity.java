package com.epamcourses.orestonatsko.model;

import com.epamcourses.orestonatsko.model.annotations.Column;
import com.epamcourses.orestonatsko.model.annotations.PrimaryKey;
import com.epamcourses.orestonatsko.model.annotations.Table;

@Table(name = "discipline")
public class DisciplineEntity {

    @PrimaryKey
    @Column(name = "id")
    private Integer id;
    @Column(name = "discipline")
    private String discipline;

    public DisciplineEntity() {
    }

    public DisciplineEntity(Integer id, String discipline) {
        this.id = id;
        this.discipline = discipline;
    }

    public DisciplineEntity(String discipline) {
        this.discipline = discipline;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDiscipline() {
        return discipline;
    }

    public void setDiscipline(String discipline) {
        this.discipline = discipline;
    }

    @Override
    public String toString() {
        return String.format("id: %-3d %s", id, discipline);
    }
}
