package com.epamcourses.orestonatsko.connection;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionManager {
//    private static final String url = "jdbc:mysql://localhost:3306";
    private static final String url = "jdbc:mysql://localhost:3306/student_db?serverTimezone=UTC&useSSL=false";
    private static final String user = "user";
    private static final String password = "password";

    private static Connection connection;

    private ConnectionManager() {
    }

    public static Connection getConnection() {
        if (connection == null) {
            try {
                connection = DriverManager.getConnection(url, user, password);
            } catch (SQLException e) {
                System.out.println("SQLException: " + e.getMessage());
                System.out.println("SQLState: " + e.getSQLState());
                System.out.println("VendorError: " + e.getErrorCode());
            }
        }
        return connection;
    }
}
