package com.epamcourses.orestonatsko.handler;

import com.epamcourses.orestonatsko.model.annotations.Column;
import com.epamcourses.orestonatsko.model.annotations.Table;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Transformer<T> {

    private Class<T> aClass;

    public Transformer(Class<T> aClass) {
        this.aClass = aClass;
    }

    public Object transformToEntity(ResultSet rs) throws SQLException {
        Object entity = null;
        try {
            entity = aClass.getConstructor().newInstance();
            if (aClass.isAnnotationPresent(Table.class)) {
                Field[] fields = aClass.getDeclaredFields();
                for (Field field : fields) {
                    if (field.isAnnotationPresent(Column.class)) {
                        Column column = field.getAnnotation(Column.class);
                        String name = column.name();
                        field.setAccessible(true);
                        Class<?> type = field.getType();
                        if (type == String.class) {
                            field.set(entity, rs.getString(name));
                        } else if (type == Integer.class) {
                            field.set(entity, rs.getInt(name));
                        }
                    }
                }
            }
        } catch (IllegalAccessException | InstantiationException |
                NoSuchMethodException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return entity;
    }


}
