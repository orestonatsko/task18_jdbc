package com.epamcourses.orestonatsko.menu;

import com.epamcourses.orestonatsko.DAO.implementation.AddressDAOImpl;
import com.epamcourses.orestonatsko.DAO.implementation.DisciplineDAOImpl;
import com.epamcourses.orestonatsko.DAO.implementation.StudentDAOImpl;
import com.epamcourses.orestonatsko.model.AddressEntity;
import com.epamcourses.orestonatsko.model.DisciplineEntity;
import com.epamcourses.orestonatsko.model.StudentEntity;
import com.epamcourses.orestonatsko.services.AddressService;
import com.epamcourses.orestonatsko.services.DisciplineService;
import com.epamcourses.orestonatsko.services.StudentService;

import java.sql.SQLException;
import java.util.*;

public class Menu {
    private Map<String, String> menu;
    private Map<String, Command> methods;
    private Scanner input;


    public Menu() {
        input = new Scanner(System.in);
        menu = new LinkedHashMap<>();
        methods = new HashMap<>();
        initMenu();

    }

    public void show() {
        String userInput;
        do {
            System.out.println("\n\t\t~~MENU~~");
            for (String key : menu.keySet()) {
                if (key.length() == 1) {
                    System.out.println(key + " - " + menu.get(key));
                }
            }
            System.out.println("Q - Quit");
            userInput = input.next();
            if (userInput.matches("^\\d")) {
                showSubMenu(userInput);
                userInput = input.next();
            }
            try {
                methods.get(userInput).execute();
            } catch (NullPointerException e) {
                //    ignore for cleaner console  view
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } while (!userInput.equalsIgnoreCase("Q"));
    }

    private void showSubMenu(String userInput) {
        System.out.println("\t~~TABLE MENU~~");
        for (String key : menu.keySet()) {
            if (key.length() != 1 && key.substring(0, 1).equals(userInput)) {
                System.out.println(key + " - " + menu.get(key));
            }
        }
    }

    private void initMenu() {

        //student table menu items
        menu.put("1", "Student Table");
        menu.put("11", "Get All Students");
        menu.put("12", "Get Student by Id");
        menu.put("13", "Get Student by Name");
        menu.put("14", "Add Student");
        menu.put("15", "Update Student");
        menu.put("16", "Delete Student");

        //address table menu items
        menu.put("2", "Address Table");
        menu.put("21", "Get All Addresses");
        menu.put("22", "Get Address by Id");
        menu.put("23", "Add Address");
        menu.put("24", "Update Address");
        menu.put("25", "Delete Address");

        //discipline table menu items
        menu.put("3", "Discipline Table");
        menu.put("31", "Get All Disciplines");
        menu.put("32", "Get Discipline by Id");
        menu.put("33", "Add Discipline");
        menu.put("34", "Update Discipline");
        menu.put("35", "Delete Discipline");


        //operations with student able
        methods.put("11", this::selectAllStudents);
        methods.put("12", this::getStudentById);
        methods.put("13", this::getStudentByName);
        methods.put("14", this::insertStudent);
        methods.put("15", this::updateStudent);
        methods.put("16", this::deleteStudent);

        //operations with address able
        methods.put("21", this::selectAllAddresses);
        methods.put("22", this::getAddressById);
        methods.put("23", this::addAddress);
        methods.put("24", this::updateAddress);
        methods.put("25", this::deleteAddress);

        //operations with discipline able
        methods.put("31", this::selectAllDisciplines);
        methods.put("32", this::getDisciplineById);
        methods.put("33", this::addDiscipline);
        methods.put("34", this::updateDiscipline);
        methods.put("35", this::deleteDiscipline);


    }

    private void deleteDiscipline() throws SQLException {
        System.out.println("Enter the id discipline  you want to delete:");
        int disciplineId = getIntegerInput();
        int count = new DisciplineService(new DisciplineDAOImpl()).delete(disciplineId);
        System.out.println("Rows deleted: " + count + ";");
    }

    private void updateDiscipline() throws SQLException {
        int disciplineId;
        String discipline;
        System.out.println("Enter old discipline id:");
        disciplineId = getIntegerInput();
        System.out.println("Enter new discipline:");
        discipline = input.nextLine();
        int count = new DisciplineService(new DisciplineDAOImpl()).update(new DisciplineEntity(disciplineId,discipline));
        System.out.println("Rows updated: " + count + ";");
    }

    private void addDiscipline() throws SQLException {
        System.out.println("Enter new discipline:");
        String discipline = input.nextLine();
        int count = new DisciplineService(new DisciplineDAOImpl()).create(new DisciplineEntity(discipline));
        System.out.println("Rows added: " + count + ";");
    }

    private void getDisciplineById() throws SQLException {
        System.out.println("Enter discipline id:");
        int disciplineId = getIntegerInput();
        DisciplineEntity discipline = new DisciplineService(new DisciplineDAOImpl()).getById(disciplineId);
        System.out.println("\t\t~~Discipline~~");
        System.out.println(discipline);
    }

    private void selectAllDisciplines() throws SQLException {
        List<DisciplineEntity> disciplines;
        disciplines = new DisciplineService(new DisciplineDAOImpl()).getAll();
        System.out.println("\t\t~~Disciplines~~");
        disciplines.forEach(System.out::println);

    }
    //------------------------Address operations-------------------------------------------------

    private void deleteAddress() throws SQLException {
        int addressId;
        System.out.println("Enter id address:");
        addressId = getIntegerInput();
        int count = new AddressService(new AddressDAOImpl()).delete(addressId);
        System.out.println("Rows deleted: " + count + ";");
    }

    private void updateAddress() throws SQLException {
        int addressId;
        int numbBuilding;
        String city;
        String street;
        System.out.println("Enter old address id:");
        addressId = getIntegerInput();
        System.out.println("Enter city");
        city = input.next();
        System.out.println("Enter street");
        street = input.next();
        System.out.println("Enter building number");
        numbBuilding = getIntegerInput();
        int count = new AddressService(new AddressDAOImpl()).update(new AddressEntity(addressId, city, street, numbBuilding));
        System.out.println("Rows updated: " + count + ";");
    }

    private void addAddress() throws SQLException {
        String city;
        String street;
        int numbBuilding;
        System.out.println("Enter new address");
        System.out.println("city:");
        city = input.next();
        System.out.println("street:");
        street = input.next();
        System.out.println("building number:");
        numbBuilding = getIntegerInput();
        int count = new AddressService(new AddressDAOImpl()).create(new AddressEntity(city, street, numbBuilding));
        System.out.println("Rows added: " + count + ";");
    }

    private void getAddressById() throws SQLException {
        System.out.println("Enter Address id:");
        int addressId = getIntegerInput();
        AddressEntity address = new AddressService(new AddressDAOImpl()).getById(addressId);
        System.out.println("\t\t~~Address~~");
        System.out.println(address);
    }

    private void selectAllAddresses() throws SQLException {
        List<AddressEntity> addresses = new AddressService(new AddressDAOImpl()).getAll();
        System.out.println("\t\t~~Addresses~~");
        addresses.forEach(System.out::println);
    }
    //------------------------Student operations-------------------------------------------------

    private void getStudentByName() throws SQLException {
        System.out.println("Enter student first name: ");
        String firstName = input.next();
        StudentEntity student = new StudentService(new StudentDAOImpl()).getByName(firstName);
        System.out.println("\t\t~~Student~~");
        System.out.println(student);
    }

    private void deleteStudent() throws SQLException {
        System.out.println("Enter student id:");
        int id = getIntegerInput();
        int count = new StudentService(new StudentDAOImpl()).delete(id);
        System.out.println("\t\t~~Rows deleted: " + count + "~~");
    }

    private void updateStudent() throws SQLException {
        StudentEntity student = new StudentEntity();
        System.out.println("Enter id of student");
        int studentId = getIntegerInput();
        student.setId(studentId);
        System.out.println("Enter student first name");
        String firstName = input.next();
        student.setFirstName(firstName);
        System.out.println("Enter student second name");
        String secondName = input.next();
        student.setSecondName(secondName);
        int count = new StudentService(new StudentDAOImpl()).update(student);
        System.out.println("\t\t~~Rows updated: " + count + "~~");
    }

    private void insertStudent() throws SQLException {
        StudentEntity student = new StudentEntity();
        System.out.println("Student:");
        System.out.println("Enter first name: ");
        String firstName = input.next();
        student.setFirstName(firstName);
        System.out.println("Enter second name: ");
        String secondName = input.next();
        student.setSecondName(secondName);
        int count = new StudentService(new StudentDAOImpl()).create(student);
        System.out.println("\t\t~~Rows added " + count + "~~");
    }

    private void getStudentById() throws SQLException {
        System.out.println("Enter student id:");
        Integer studentId = getIntegerInput();
        StudentEntity student = new StudentService(new StudentDAOImpl()).getById(studentId);
        System.out.println("\t\t~~Student~~");
        System.out.println(student);
    }

    private void selectAllStudents() throws SQLException {
        List<StudentEntity> students = new StudentService(new StudentDAOImpl()).getAll();
        System.out.println("\t\t~~Students~~");
        students.forEach(System.out::println);
    }

    private Integer getIntegerInput() {
        Integer id = null;
        while (true) {
            try {
                id = input.nextInt();
            } catch (NumberFormatException e) {
                System.out.println("Incorrect Input!");
            }
            break;
        }
        return id;
    }
}