package com.epamcourses.orestonatsko.services;

import com.epamcourses.orestonatsko.DAO.DisciplineDAO;
import com.epamcourses.orestonatsko.model.DisciplineEntity;

import java.sql.SQLException;
import java.util.List;

public class DisciplineService implements Service<DisciplineEntity, Integer> {

    private DisciplineDAO disciplineDAO;

    public DisciplineService(DisciplineDAO disciplineDAO) {
        this.disciplineDAO = disciplineDAO;
    }

    @Override
    public List<DisciplineEntity> getAll() throws SQLException {
        return disciplineDAO.getAll();
    }

    @Override
    public DisciplineEntity getById(Integer id) throws SQLException {
        return disciplineDAO.getById(id);
    }

    @Override
    public int create(DisciplineEntity entity) throws SQLException {
        return disciplineDAO.create(entity);
    }

    @Override
    public int update(DisciplineEntity entity) throws SQLException {
        return disciplineDAO.update(entity);
    }

    @Override
    public int delete(Integer id) throws SQLException {
        return disciplineDAO.delete(id);
    }
}
