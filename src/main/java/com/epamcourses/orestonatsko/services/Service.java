package com.epamcourses.orestonatsko.services;

import java.sql.SQLException;
import java.util.List;

public interface Service<T, ID> {
    List<T> getAll() throws SQLException;
    T getById(ID id) throws SQLException;
    int create(T entity) throws SQLException;
    int update(T entity) throws SQLException;
    int delete(ID id) throws SQLException;
}
