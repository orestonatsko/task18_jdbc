package com.epamcourses.orestonatsko.services;

import com.epamcourses.orestonatsko.DAO.StudentDAO;
import com.epamcourses.orestonatsko.model.StudentEntity;

import java.sql.SQLException;
import java.util.List;

public class StudentService implements Service<StudentEntity, Integer> {

    private StudentDAO dataAccessObject;

    public StudentService(StudentDAO dao) {
        this.dataAccessObject = dao;
    }

    @Override
    public List<StudentEntity> getAll() throws SQLException {
        return dataAccessObject.getAll();
    }

    @Override
    public StudentEntity getById(Integer id) throws SQLException {
        return dataAccessObject.getById(id);
    }

    @Override
    public int create(StudentEntity student) throws SQLException {
        return dataAccessObject.create(student);
    }

    @Override
    public int update(StudentEntity student) throws SQLException {
        return dataAccessObject.update(student);
    }

    @Override
    public int delete(Integer id) throws SQLException {
        return dataAccessObject.delete(id);
    }

    public StudentEntity getByName(String name) throws SQLException {
        return dataAccessObject.getByName(name);
    }
}
