package com.epamcourses.orestonatsko.services;

import com.epamcourses.orestonatsko.DAO.AddressDAO;
import com.epamcourses.orestonatsko.model.AddressEntity;

import java.sql.SQLException;
import java.util.List;

public class AddressService implements Service<AddressEntity, Integer> {
    private AddressDAO addressDAO;

    public AddressService(AddressDAO addressDAO) {
        this.addressDAO = addressDAO;
    }

    @Override
    public List<AddressEntity> getAll() throws SQLException {
        return addressDAO.getAll();
    }

    @Override
    public AddressEntity getById(Integer id) throws SQLException {
        return addressDAO.getById(id);
    }

    @Override
    public int create(AddressEntity address) throws SQLException {
        return addressDAO.create(address);
    }

    @Override
    public int update(AddressEntity address) throws SQLException {
        return addressDAO.update(address);
    }

    @Override
    public int delete(Integer id) throws SQLException {
        return addressDAO.delete(id);
    }
}
